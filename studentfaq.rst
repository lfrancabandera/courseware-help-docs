Student FAQ:
============
.. _studentfaq:

Welcome students to your frequently asked questions page about Credo Courseware! Take a look at the list of questions below and if you cannot find your answer, please contact Credo Support at support@credoeducation.com.

    - `Course Not Showing on Dashboard`_
    

Course Not Showing on Dashboard
-------------------------------

**Question:** I am trying to find my course on the Credo website, I have registered for my account and activated it with the email I was sent, but the "My Courses" section is empty as well as the "Find Courses" section when I try to find my course. What do I do? 

**Possible Answers:** 

*Already Registered:* If you've already registered and are not seeing any courses in your My Courses list, go to your institution homepage link and then log in to access the course. your institution homepage will be in the following format: https://collegename.credocourseware.com/

*Not Registered:* If you have not yet registered for the course, use the course registration link if you have one. Complete the following steps to enroll:

* Click on the registration link.
* On the Course Description page select the “Enroll” button.
* Complete the registration form by providing your e-mail, full name, public username, and password. Please also be sure to tick the Terms of Service box.
* Once you have completed the form you will see a screen that says you are not enrolled in any courses. Please check your email at this point as you will receive an email from Credo containing an activation link.
* Please click the activation link to begin using your Credo access. Your course will appear in your My Courses list after you click this link.

*No Registration Link:* If you do not have a registration link and still don't see any courses, it is possible that your course has not started yet and is hidden, or that your instructor needs to manually enroll you. In this case, you will need to get in touch with your instructor.

:ref:`search`