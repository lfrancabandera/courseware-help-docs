Introduction
=============

This set of documentation will help you in your administration of Credo's Courseware.

We recommend starting with the QuickStart Guide, and then moving onto the more in-depth tutorials, depending on your needs.


