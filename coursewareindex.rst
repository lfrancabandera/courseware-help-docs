
Welcome to the Courseware Help Center
=====================================
.. _coursewareindex:

	This is your one-stop-shop for everything you might need. This help portal is separated into a few different areas.

	Look at the main topic areas below to find your answer.

.. toctree::
    Technical Information <technical.rst>
    Student FAQ <studentfaq.rst>
		Adding Images <adding-images.rst>
		Adding Text <adding-text.rst>
		Creating an SRT Transcript File <creating-an-srt-file.rst>

:ref:`search`
