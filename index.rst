Welcome to the Credo Education Table of Contents!
=================================================
.. _toc:
	This is your one-stop-shop for everything you might need. This is the master Table of Contents for both Credo Courseware and the InfoLit Modules.
	
	Look at the topic areas below to find your answer.
    
.. toctree::
        Your Credo Support Team <intro.rst>
        InfoLit Modules Help <modulesindex.rst>
        Credo Courseware Help <coursewareindex.rst>
        Glossary <glossary.rst>

:ref:`search` 
