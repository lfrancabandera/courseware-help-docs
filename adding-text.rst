Adding Text
===========

You can add text to any page of your modules. To do so, go to the page where you would like to add text and follow these steps.

**Step 1:**  To create a text box, you first much click on the green "html" button.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/text-1.png
   :alt:
   :width: 100%

**Step 2:**  Next, click on the "text" link.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/text-2.png
   :alt:
   :width: 100%

**Step 3:**  Your text box is created. You must click on the "Edit" button to make changes.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/text-3.png
   :alt:
   :width: 100%

**Step 4:**  In this simple text editor, you can bold, italicize, underline, and change the color of the font. You can create bullet points, lists, and block quotes. To create a link, click on the chain icon. You can write your text directly into the blank text space. When you are finished, click "save."

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/text-4.png
   :alt:
   :width: 100%

**Step 5 (Optional):**  If you prefer to edit your text in plain html, you can click on the "html" button on the top right of the text editor. That will bring you to the HTML source code where you can write directly in raw html.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/text-5.png
   :alt:
   :width: 100%
