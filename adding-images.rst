Adding Images
=============

There are a few different ways to add images to your modules: 1) If your images are hosted on a server (and have an image URL) or 2) if you need to upload your images from your computer.

**If you have the image URL:**  If you already have an image on a server and have the URL, you can simply click on the raw html button and use the basic html image code. It is highly recommended that you add an alternate label ("alt") for screenreaders and accessibility.

\<img src="http://imageurl" width="100%" alt="image of a book" /\>

**If you just have the image file:**

Step 1: Under the top navigation dropdown for "Content," click own "Files\&Uploads."

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-1.png
   :alt: image of the top navigation drop down with files and uploads in blue
   :width: 40%

Step 2: To the top right, click on the green "Upload new file" button.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-2.png
   :alt: image of the green button with white text that says upload new file
   :width: 50%

Step 3: In the upload window, click on the blue "Choose File" button and select the image file from your computer.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-3.png
   :alt: image of the upload window and a blue choose file button
   :width: 50%

Step 4: The button will turn green and it will say "upload completed." Below that is a URL that beings "/static" copy that URL to your computer clipboard.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-4.png
   :alt: image of the upload window and a green button that says upload complete with a url below it that is highlighted
   :width: 70%

Step 5: Inside the text page where you want to insert the image, click on the "image" button in the html editor top editing buttons.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-4b.png
   :alt: image of the editing buttons in the html editor
   :width: 60%

Step 6: Paste the image URL that you copied earlier into the source field. Add whichever settings you want your image to contain.

.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/image-5.png
   :alt: image of the image settings box
   :width: 60%
