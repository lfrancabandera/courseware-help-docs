Creating an SRT File
====================

If you add in a new video into your course, it is important to also create a transcript file for accessibility. By creating and uploading a transcript, the video player will be able to show closed captions and offer students a timed transcript.

The transcripts need to be in a .srt, or SubRip, file - a specific type of timed transcript file.

**Step 1:**  To create a timed transcript, open a text editor.

**Step 2:**  Each timed caption item must follow the same format and consists of four parts:



* A number that shows where the caption is in the sequence
* The time of the caption (in hour:minutes:seconds,miliseconds)
* The caption text
* A blank line showing the start of a new caption



.. image:: //cdn.credoreference.com/client-7446/help-center/2016/studio-training/transcript.png
   :alt: image of a transcript number, time, and text.
   :width: 100%

Note: The time format must be hh:mm:ss,sss. So if a caption starts at 3 minutes and 5 seconds exactly, you must write it as 00:03:05,000. hours, minutes, and seconds are always written with two characters (09, not 9) and milliseconds are always written with three characters (001)

**Step 3:**  Once you've completed creating all the timed caption items you need, save the transcript file as an SRT file. Click Save As on the text editor, enter a file name and add the file extension ".srt" . There can be no spaces in the file name, so use dashes instead of spaces (e.g. primary-sources.srt).

**Step 4:**  You can then upload that .srt file into the video player. If there is an issue with your formatting in the file, the video player will not accept your file. In that case, go back into your file and try to locate the error.

**Common errors in .srt files:**

* Missing spaces in the time (00:00:19,000-->00:00:23,000) - note the missing spaces on each side of the arrow.
* Not using a comma before the milliseconds (00:00:09:000)
* Forgetting to add the milliseconds (00:00:09)
* Skipping a sequential number (going from 1 to 3)
* An accidental new line space between one of the three main items (the number, time, or caption)
