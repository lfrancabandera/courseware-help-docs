Glossary of Terms
=================
.. _glossary:
   
   `A`_ `B`_ `C`_ `D`_ `E`_ `F`_ `G`_ `H`_ `I`_ `J`_ `K`_ `L`_ `M`_ `N`_ `O`_ `P`_ `Q`_ `R`_ `S`_ `T`_ `U`_ `V`_ `W`_ `X`_ `Y`_ `Z`_

.. glossary::

A
-
**Accessibility**
`````````````````
    The ability for an online website or activity to be usable by users of various disabilities and challenges. Often includes alt text or labels, as well as keyboard commands.
        *Example:* This course is accessible.
        
B
-

C
-
**Chapter**
```````````
    A top-level organizational construct within the LMS. See also `Subsection`_
        *Example:* Next, click on Chapter 2 in the left-hand navigation.
    
**Component**
`````````````
    A lower-level organizational construct within Studio - used in both Credo Courseware and InfoLit Modules to denote the items on a single page (inclusive of questions, text, or videos). See also `HTML Component`_, `Question Component`_, and `Video Component`_
        *Example*: Create an HTML text component and then a Question component right below it.
 
D
-
**Django**
``````````
    The administrative portion of EdX, where LTI, Users, and Instructor Permissions are handled. 
        *Example*: Can you go into Django and check the email for this user?

E
-
**EdX**
```````
    The backbone of the Credo Education product platform. Credo uses a customized Open EdX platform and gives programming back to the EdX community.    
        *Example:* The Credo Courseware is using the EdX platform.

F
-

G
-
**Gradebook**
`````````````
    The place where student grades are saved and can be viewed all in one place. Can be found on the Instructor tab by clicking on Student Admin.
        *Example:* My quiz score is in the gradebook.

H
-
**HTML Component**
``````````````````
    The basic text editor for placing text, links, and images on a page. See also `Component`_
        *Example:* Create a new HTML component for the introductory text.

I
-
**Instructor Permissions**
``````````````````````````
    Within Django, the place where Credo employees can manage which items instructors see on their Instructor tab. See also `Django`_
        *Example:* Go set the instructor permissions for Dr. Smith.

J
-

K
-

L
-
**Lesson**
``````````
    A top-level organizational construct within the student-facing LMS. See also `Section`_
        *Example:* Now click on the Research Process Lesson in the left-hand navigation.

**LMS**
```````
    Learning Management System, when used in reference to Credo Education products, it is referring to the student interface. See also `EdX`_
        *Example:* Does it happen in Studio or in the LMS?

M
-
**Module**
``````````
    A lower-level organizational construct within the LMS. See also `Section`_
        *Example:* Click on the Getting Started with Research Module.

N
-
**Numerical Input**
```````````````````
    A question component where it asks the student to complete a mathematical equation and type in a single number.
        *Example:* For the accounting course, we used numerical input questions for the quizzes.

O
-
**ORA**
```````
    Open Response Assessment is a question type that asks reflective questions and allows students to type into a free text box and upload dcuments or images. ORAs can be self-graded, peer-graded, or staff-graded, which is setermined in the question settings.
        *Example:* Did you download the ORA grade report?

P
-
**Page**
````````
    A lower-level organizational construct within the LMS. See also `Unit`_
        *Example:* Page three includes a table.

Q
-
**Question Component**
``````````````````````
    A lower-level organizational construct within Studio. See also `Component`_
        *Example:* Add a question component to the page.

R
-
**Rubric**
``````````
    The grading scheme for an Open Response Assessment. See also `ORA`_
        *Example:* Here is the rubric for the ORA.

S
-
**Screenreader**
````````````````
    Software that reads the items on a website to the user and helps the visually impaired to read and understand websites. See also `Accessibility`_.
        *Example:* This page is screenreader-friendly. 

**Section**
```````````
    A top-level organizational construct within Studio. See also `Lesson`_ and `Module`_
        *Example:* Create a new section for the lesson.
    
**Studio**
``````````
    The editing interface of the Credo Education platform. See also `EdX`_
        *Example:* To make edits to your course, so to Studio.
    
**Subsection**
``````````````
    A top-level organizational construct within Studio - used in Credo Courseware as "chapter-level" and in InfoLit Modules as "tutorial-level" or "video-level." See also `Chapter`_
        *Example:* Create a new subsection.

T
-
**Text**
````````
    A lower-level organizational construct within the LMS. See also `Component`_ and `HTML Component`_
        *Example:* Create a text component on the page.

U
-
**Unit** 
````````
    A lower-level organizational construct within Studio. See also `Page`_
        *Example:* Create a new unit for the next page.
    
V
-
**Video Component**
```````````````````
    A basic level organizational construct within Studio. Adds a video to a page. See also `Component`_
        *Example:* Add a video component to page six.

W
-

X
-

Y
-

Z
-

:ref:`search`