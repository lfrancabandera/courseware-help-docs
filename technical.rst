Technical Information
=====================
.. _technical:

This page explains the accessibility of Credo Education products, how Credo maintains student data privacy and security, as well as troubleshooting information and minimum browser requirements. Choose your topic from the index below:

        - `Accessibility`_
        
        - `Data Privacy & Security`_
        
        - `Minimum Browser Requirements`_



Accessibility
-------------

The Credo Education products have been through rigorous accessibility testing and are both WCAG 2.0 and Section 508 compliant. You can download the VPAT below.

**Voluntary Product Accessibility Template:**
* `Download the Courseware VPAT <https://cdn.credoreference.com/client-7446/help-center/2016/Voluntary-Product-Accessibility-Template-Courseware-3.0.pdf>`_ as a PDF.
* `Download the InfoLit Modules VPAT <https://cdn.credoreference.com/client-7446/help-center/2016/vpat-ILModules2.0-updated.pdf>`_  as a PDF.

**Videos:** The video player used in Credo Education products includes moveable closed captions as well as downloadable, timed transcripts. Students may also decrease or increase the narration speed to fit their cognitive learning style. For students with slow internet or bandwidth who are unable to stream videos, they may read or print the video transcript.

**Images:** Every image includes an alt text for screenreaders and students who may not be able to see the image. This includes both images with text as well as purely graphic images.

**Drag and Drop Questions:** The drag and drop questions use target zone labels and drag item descriptions for learners who cannot access the target zone or drag items visually. If you are using a keyboard only, the drag and drop questions include keyboard instructions. To complete the problem using only your keyboard:

* Use "Tab" and "Shift-Tab" to navigate between items and zones.
* Press "Enter", "Space", "Ctrl-m", or "⌘-m" on an item to select it for dropping, then navigate to the zone you want to drop it on.
* Press "Enter", "Space", "Ctrl-m", or "⌘-m" to drop the item on the current zone.
* Press "Esc" if you want to cancel the drop operation (for example, to select a different item).

Back to Top - `Technical Information`_

Data Privacy & Security
-----------------------

Credo understands that data privacy and security is of the foremost importance. For that reason, data sent via the web is encrypted by https provided that you have configured use of https within your LMS. Data at rest is unencrypted. Credo does not store any sensitive data as defined by PII, FERPA, PCI and/or HIPAA. 

If you have any questions, Credo’s primary Customer Support location is in Boston, MA with support hours from 9 am to 5 pm EST. To reach customer support email `support@credoreference.com <mailto:support@credoeducation.com>`_.

*Data Privacy:* Credo protects the privacy of our Licensees’ student education records in conformance with the Family Educational Rights and Privacy Act ("FERPA"), where FERPA is applicable.

*Data Management:* The Credo Courseware solution is deployed in a highly available configuration with full redundancy of servers at every tier. Routine database backups are taken every 24 hours in addition to real-time transaction logs allowing for point-in-time-recovery going back 31 days. Credo Courseware hosting and data storage is located within the United States.

*Stability:* Credo Courseware SLA is 99.9% uptime with the exception of any maintenance. Any maintenance involving downtime will be scheduled and customers will be notified in advance. Credo has a test environment which allows for testing LTI and SSO integration before launch. The test environment is hosted on a separate server from the production environment.

*Integration & Releases:* Credo uses an Agile development cycle. Maintenance items are released continuously, as they are completed. New feature updates are released between academic semesters to avoid disruption. All code in our application must pass integration testing before deployment. We perform regular interface testing to ensure functionality is working properly after updates.

Back to Top - `Technical Information`_

Minimum Browser Requirements
----------------------------

Credo’s primary Customer Support location is in Boston, MA with support hours from 9 am to 5 pm EST. To reach customer support email support@credoreference.com.

*Browser Requirements*

Credo's learning platform supports the current versions of:
* Chrome
* Safari
* Firefox
* Microsoft Edge
* Internet Explorer 11
 
*Maintenance & Downtime*

Credo Courseware SLA is 99.9% uptime with the exception of any maintenance. Any maintenance involving downtime will be scheduled and customers will be notified in advance.

*Learning Management Systems Supported*
* Blackboard: Blackboard Learn v. 9.1, Service Pack 9 or later
* D2L: Brightspace Platform 10.3 or later
* Canvas: Any version
* Moodle: Version 2.6.5 or later
* LTI: Version LTI 1.1


Back to Top - `Technical Information`_
:ref:`search`